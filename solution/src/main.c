#include "bmp.h"
#include "file.h"
#include "image.h"
#include "transformation.h"
#include <stdio.h>

static const char* write_status_output[] = {
	[WRITE_OK] = "Content written succesfully\n",
	[WRITE_ERROR] = "An error occurred while writing image contents\n",
	[FILE_WRITE_ERROR] = "An error occurred while writing into the file\n",
	[SUCCESSFUL_FILE_WRITE] = "File successfully written\n",
	[WRITE_FILE_OPEN_ERROR] = "Error occured while opening the file\n"
};

static const char* read_status_output[] = {
	[READ_OK] = "Content successfully read\n",
	[READ_INVALID_HEADER] = "Error while reading the header\n",
	[SUCCESSFUL_FILE_READ] = "File successfully read\n",
	[FILE_READ_ERROR] = "Error occured while reading the file\n",
	[READ_FILE_OPEN_ERROR] = "Error occured while opening the file\n"
};	

static const char* app_status_output[] = {
	[ARGS_WRONG] = "Incorrect command format, please input source and target images\n",
	[ARGS_CORRECT] = "App launched!\n",
	[APP_SUCCESS] = "App finished successfully\n"
};

/*
static void standart_output(const char* status) {
	fprintf(stdout, "%s", status);
}
*/

static void standart_error_output(const char* status) {
	fprintf(stderr, "%s", status);
}


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if(argc != 3) {
    	standart_error_output(app_status_output[ARGS_WRONG]);
    } else {
        standart_error_output(app_status_output[ARGS_CORRECT]);
    	struct image source = {0};
    	struct image result = {0};
    	enum read_status read_image_status = get_image(argv[1], &source);
    	if(read_image_status >= 2) {
    		standart_error_output(read_status_output[read_image_status]);
    		image_delete(source);
    		return 1;
    	} else standart_error_output(read_status_output[read_image_status]);
        result = rotate(source);
    	enum write_status write_image_status = write_image(argv[2], &result);
    	if(write_image_status >= 2) {
    		standart_error_output(write_status_output[write_image_status]);
    		image_delete(result);
			image_delete(source);
    		return 1;
    	} else standart_error_output(write_status_output[write_image_status]);
    	image_delete(result);
    	image_delete(source);
    	standart_error_output(app_status_output[APP_SUCCESS]);
    }
    return 0;
}
