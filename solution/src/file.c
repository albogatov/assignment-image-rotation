#include "file.h"

enum read_status get_image(char* const filename, struct image* const image) {
	FILE *filePointer;
	filePointer = fopen(filename,"rb");
	if (filePointer == NULL)
		return READ_FILE_OPEN_ERROR;
	enum read_status read_bmp_status = from_bmp(filePointer, image);
	if(read_bmp_status != READ_OK)
		return read_bmp_status;
	fclose(filePointer);
	return SUCCESSFUL_FILE_READ;
}

enum write_status write_image(char* const filename, struct image* const image){
	FILE *filePointer;
	filePointer = fopen(filename,"wb");
	if (filePointer == NULL)
		return WRITE_FILE_OPEN_ERROR;
	enum write_status write_bmp_status = to_bmp(filePointer, image);
	if(write_bmp_status != WRITE_OK)
		return write_bmp_status;
	fclose(filePointer);
	return SUCCESSFUL_FILE_WRITE;
}
