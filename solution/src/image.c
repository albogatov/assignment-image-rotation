#include "image.h"

struct image image_create(size_t width, size_t height) {
	struct image img = {0};
	img.width = width;
	img.height = height;
	return img;
}


void image_delete(struct image img) {
	free(img.data);
}
