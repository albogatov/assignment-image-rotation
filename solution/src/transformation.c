#include "transformation.h"

static uint64_t calc_x(size_t i, struct image source) {
	return source.height - 1 - i / source.width;
}

static uint64_t calc_y(size_t i, struct image source) {
	return i%source.width;
}

struct image rotate(struct image source) {
	uint64_t x;
	uint64_t y;
	uint64_t pixel_amount = source.height*source.width;
	struct image result = image_create(source.height, source.width);
	struct pixel* rotated_pixels = malloc(sizeof(struct pixel)*pixel_amount);
	struct pixel* original_pixels = source.data;
	for(size_t i = 0; i < pixel_amount; i++) {
		x = calc_x(i, source);
		y = calc_y(i,source);		
		rotated_pixels[y*source.height+x] = original_pixels[i];
	}
	result.data = rotated_pixels;
	return result;
}
