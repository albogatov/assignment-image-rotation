#ifndef STATUS
#define STATUS
/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  SUCCESSFUL_FILE_WRITE = 1,
  WRITE_ERROR = 2,
  FILE_WRITE_ERROR = 3,
  WRITE_FILE_OPEN_ERROR = 4
  /* коды других ошибок  */
};
/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  SUCCESSFUL_FILE_READ = 1,
  READ_INVALID_HEADER = 2,
  FILE_READ_ERROR = 3,
  READ_FILE_OPEN_ERROR = 4
  /* коды других ошибок  */
};

enum app_status {
  ARGS_WRONG = 0,
  ARGS_CORRECT = 1,
  APP_SUCCESS = 2
};
#endif

