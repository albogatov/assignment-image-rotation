#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#ifndef IMAGE_STRUCTS
#define IMAGE_STRUCTS
struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};
#endif

struct image image_create(size_t width, size_t height);


void image_delete(struct image img);
