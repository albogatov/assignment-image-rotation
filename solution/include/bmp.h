#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "image.h"
#include "status.h"

#ifndef BMP
#define BMP
struct __attribute__((__packed__)) bmp_header{
    uint16_t bfType;              // "BM" (0x42, 0x4D)
    uint32_t bFileSize;              // file size
    uint32_t bfReserved;         // not used (0)
    uint32_t bOffBits;            // offset to image data (54B)
    uint32_t biSize;          // DIB header size (40B)
    uint32_t biWidth;             // width in pixels
    uint32_t biHeight;            // height in pixels
    uint16_t biPlanes;            // 1
    uint16_t biBitCount;               // bits per pixel (24)
    uint32_t biCompression;       // compression type (0/1/2) 0
    uint32_t biSizeImage;        // size of picture in bytes, 0
    uint32_t biXPelsPerMeter;             // X Pixels per meter (0)
    uint32_t biYPelsPerMeter;             // X Pixels per meter (0)
    uint32_t biClrUsed;        // number of colors (0)
    uint32_t  biClrImportant;  // important colors (0)
};
#endif

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

